<!DOCTYPE html>
<html lang="en">

<head>
  <title>EAD Store</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="M4CSS.css">
  <script>
    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }

  </script>
</head>

<body>

  <div class="row">
  </div>
    <img src="EAD.png" style="float: left; margin-left:30px;margin-top: -35px;" width="130px" weight="120px"> 
    <div class="but" style="float: right; margin-top:-50px;">
   
      
        <button onclick="document.getElementById('id01').style.display='block'" id='btn1'>Login</button>
      <button onclick="document.getElementById('id012').style.display='block'" style='width:auto;'id='btn2'>Register</button>
    </div>
    <div id="id01" class="modal">

      <form class="modal-content animate" action="Login.php" method="post">
        <div class="imgcontainer">
          <span onclick="document.getElementById('id01').style.display='none'" class="close"
            title="Close Modal">&times;</span>

        </div>
        <div class="modheader">
          <h3 style="margin-left: 16px;">Login</h3>
          <hr>
        </div>
        <div class="container">
          <label for="uname"><b>Email Address</b></label>
          <input type="email" placeholder="Enter Email" name="email" required>

          <label for="psw"><b>Password</b></label>
          <input type="password" placeholder="Enter Password" name="psw" required>

        </div>

        <div class="footer">
          <hr>
          <div class="container">

            <button type="submit" style="float: right; border-radius:5px;">Login</button>

            <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn"
              style="float: right; margin-right: 10px;border-radius:5px;">Close</button>


          </div>
        </div>

      </form>

    </div>
    <div class="but">

    </div>
    <div id="id012" class="modal">

      <form action="Register.php" class="modal-content animate"  method="POST">
        <div class="imgcontainer">
          <span onclick="document.getElementById('id012').style.display='none'" class="close"
            title="Close Modal">&times;</span>

        </div>
        <div class="modheader">
          <h3 style="margin-left: 16px;">Register</h3>
          <hr>
        </div>
        <div class="container">
          <label for="mail"><b>Email Address</b></label>
          <input type="email" placeholder="Enter Email" name="pesan" required>
          <label for="uname"><b>Username </b></label>
          <input type="text" placeholder="Enter Username" name="uname" required>
          <label for="mob"><b>Mobile Number </b></label>
          <input type="number" placeholder="Enter Phone" name="mob" required>
          <label for="psw"><b>Password</b></label>
          <input type="password" placeholder="Enter Password" name="psw" required>
          <label for="conpsw"><b>Confirm Password</b></label>
          <input type="password" placeholder="Confirm Password" name="conpsw" required>


        </div>
        <div class="footer">
          <hr>
          <div class="container">
            <button type="submit" name="submit" style="float: right; margin-right: 10px;border-radius:5px;">Register</button>
            <button type="button" onclick="document.getElementById('id012').style.display='none'" class="cancelbtn"
              style="float: right; margin-right: 10px;border-radius:5px;">Close</button>
          </div>
        </div>
      </form>
    </div>

  
  <hr>
  <div class="jumbotron" style="margin-left: 260px; margin-right: 225px;">
    <h1>Hello Coders</h1>
    <p>Welcome to our store, please take a look for the product you might buy.</p>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <img src="web.png" alt="logo web" style="width:100% ;">
          <h5 style="text-align: left; margin-left: 10px;padding-top: 20px;"><b>Learning Basic Web Programming</b></h5>
          <p class="price"><b>Rp.210.000,-</b></p>
          <p style="text-align: left;margin-left: 10px;">Want to be able to make a website? Learn basic components such
            as HTML, CSS, JavaScript in this class curriculum.</p>


          <hr>
          <p><button name="python">Buy</button></p>
        </div>

      </div>
      <div class="col-sm-4">
        <div class="card">
          <img src="java.png" alt="logojava" style="width:100%">
          <h5 style="text-align: left; padding-top: 20px;margin-left: 10px;"><b>Starting Programming in Java</b></h5>
          <p class="price"><b>Rp.150.000,-</b></p>
          <p style="text-align: left; margin-left: 10px;">Learn Java Language for you who want to learn the most popular
            Object Oriented Proramming (PBO) concepts to developing applications</p>
          <hr>
          <p><button name="java">Buy</button></p>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card" >
          <img src="phython1.png" alt="logopython" style="width:100%">
          <h5 style="text-align: left;padding-top: 20px;margin-left: 10px;"><b>Starting Programming in Phyton</b></h5>
          <p class="price"><b>Rp.200.000,-</b></p>
          <p style="text-align: left;margin-left: 10px;">Learn Python - Fundamental various current industry trends Data
            Science, Machine Learning, Infrastructure Management.</p>
          <hr>
          <p><button name="python">Buy</button></p>
        </div>
      </div>

    </div>
  </div>

  <div class="jumbotron1 text-center" style="margin-bottom:0">
    <p>&copy;EAD STORE</p>
  </div>

</body>

</html>