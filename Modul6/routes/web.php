<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/AddNewPost', 'AddNewPostController@index')->name('AddNewPost');
Route::get('/Profile', 'ProfileEditController@index')->name('Profile');
Route::get('/EditProfile', 'ProfileEditController@edit')->name('EditProfile');
Route::post('/Profile/update', 'ProfileEditController@updateProfile')->name('EditProfile');
Route::get('likeposts/{id}','HomeController@like')->name('Like');
Route::get('/Detailpost/{Detailpost}', 'HomeController@specific')->name('Detailpost');
Route::get('/comment', 'HomeController@addkomen')->name('comment');
Route::post('/Home', 'HomeController@store')->name('Home');
Route::resource('AddNewPost','AddNewPostController');
