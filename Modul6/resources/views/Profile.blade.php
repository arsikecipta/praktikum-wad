
@extends('layouts.app')
@section('content')

    <div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img src="{{Auth::user()->avatar}}" alt="gambar" class="rounded-circle w-100">
        </div>
        <div class="col-9 pt-5">
    
            <div class="d-flex justify-content-between align-items-baseline">
                <div class="d-flex align-items-center pb-3">
                    <div class="h4">{{Auth::user()->name}} </div>
                </div>
                  <a href="{{route('AddNewPost.index')}}">Add New Post</a>  
            </div>
         
            
          
                <a href="/EditProfile">Edit Profile</a>
            
            <div class="d-flex">
                <div class="pr-5"><strong> ...</strong> posts</div><br>
               <p>{{Auth::user()->title}}</p><br><br>
               <p>{{Auth::user()->description}}</p>
               <p>{{Auth::user()->url}}</p>
            </div>
            <div class="pt-4 font-weight-bold"></div>
            <div></div>
            
            <div><a href=""></a></div>
        </div>
    </div>

    <div class="row pt-5">
            </div>
</div>

@endsection