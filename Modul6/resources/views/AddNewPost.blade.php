@extends('layouts.app')

@section('content')

<div class="row">
            <div class="col-8 offset-2">
 
                <div class="row">
                    <h1>Add New Post</h1>
                </div>
                <label for="caption" class="col-md-4 col-form-label">Post Caption</label>
<form action="{{route('AddNewPost.store')}}" method="put" enctype="multipart/form-data">
    {{csrf_field()}}
    
    <input type="hidden" value="{{Auth::id()}}" name="id"><br>
    <input type="text" name="caption"> <br><br>
    <input type="file" name="foto"> <br><br>
    <input type="submit" value="new post">
    
</form>

@endsection