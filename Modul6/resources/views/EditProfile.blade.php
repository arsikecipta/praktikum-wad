@extends('layouts.app')
@section('content')

<form action="/Profile/update" method="post" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="Id" value="{{Auth::user()->id}}">
    <div class="row">
        <div class="col-8 offset-2">

            <div class="row">
                <h1>Edit Profile</h1>
            </div>
            <div class="form-group row">
                <label for="title" class="col-md-4 col-form-label">Title</label>

                <input id="title" type="text" class="form-control" name="title" value="" autocomplete="title" autofocus>

            </div>

            <div class="form-group row">
                <label for="description" class="col-md-4 col-form-label">Description</label>

                <input id="description" type="text" class="form-control" name="description" value=""
                    autocomplete="description" autofocus>

            </div>

            <div class="form-group row">
                <label for="url" class="col-md-4 col-form-label">URL</label>

                <input id="url" type="text" class="form-control" name="url" value="" autocomplete="url" autofocus>

            </div>

            <div class="row">
                <label for="image" class="col-md-4 col-form-label">Profile Image</label>

                <input type="file" class="form-control-file" id="image" name="image">

            </div>

            <div class="row pt-4">
                <button class="btn btn-primary">Save Profile</button>
            </div>

        </div>
    </div>
</form>
@endsection