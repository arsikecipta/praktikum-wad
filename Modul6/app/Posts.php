<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    //
     protected $table='posts';
     protected $fillable = ['user_id','caption', 'image'
    ];   

    public function comment(){
        return $this->hasMany('App\Comment','post_id','id');
    }
    public function users(){
        return $this->belongsto('App\User','user_id');
    }
}
