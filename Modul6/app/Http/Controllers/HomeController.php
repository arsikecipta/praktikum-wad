<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Posts;
use App\User;
use App\Comment;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts= Posts::get();        
        return view('home',[ 'post'=>$posts]);
       
    }
    public function like($id){
        posts::find($id)->increment('likes');
        return redirect('home');
    }
    public function specific($id){
    
     
        $posts = posts::find($id);
        return view('Detailpost',['posts'=>$posts]);
      
    }

    
    public function store(){
    dd(request()->all());  
    }
 public function addkomen(Request $request){
     $comment = new Comment();
     $comment->user_id =$request->user_id;
     $comment->post_id =$request->post_id;
     $comment->comment =$request->komen;
     $comment->save();
     return redirect('home');
}
public function editprofil(){
    return view('EditProfile');
}

}