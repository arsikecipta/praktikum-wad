<?php

use Illuminate\Database\Seeder;


class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       
        DB::table('posts')->insert([
            'user_id'=>1,
            'caption'=>'EAD adalah Enterprise Application Development, sebuah laboratorium dibawah naungan Fakultas Rekayasa Industri Prodi Sistem Informasi',
            'image'=>'img/ead.PNG',
        ]);
        DB::table('posts')->insert([
            'user_id'=>1,
            'caption'=>'Ka dima ajak aku ke jepang dong',
            'image'=>'img/jepang.jpg',
        ]);
       
    }
}
